import data from "../data";
import { User } from "../models/user";
import * as config from "./config";

let users = [];

export default io => {
    io.on("connection", socket => {
        const username = socket.handshake.query.username;

        let toDelete = true;

        if (users.find(user => user.username === username)) {
            socket.emit('user_error', username);
            toDelete = false;
        } else {
            socket.emit('add_users', users.map(user => user.username));
            users.push(new User(username, false));
            socket.emit('add_myself', username);
            socket.broadcast.emit('add_user', username);
        }

        socket.on('is_ready', username => {
            let index = users.findIndex(user => user.username === username);
            let user = users[index];
            user.isReady = !user.isReady;
            io.emit('ready', user);
            if (users.every(user => user.isReady === true)) {
                const random_text_i = Math.floor(Math.random() * Math.floor(data.texts.length));
                io.emit('at_timer_start', random_text_i);

                let counter = config.SECONDS_TIMER_BEFORE_START_GAME;
                let interval = setInterval(() => {
                    io.emit('timer_before_start', counter--);
                    if (counter < 0) {
                        clearInterval(interval);
                        let counter2 = config.SECONDS_FOR_GAME;
                        let interval2 = setInterval(() => {
                            io.emit('game_timer', --counter2);
                            if (counter2 === 0) {
                                clearInterval(interval2);
                            }
                        }, 1000)
                    }
                }, 1000)
            }
        });

        socket.on('press_key', username => {
            io.emit('update_progress', username);
        });

        socket.on('finished', username => {
            let index = users.findIndex(user => user.username === username);
            let user = users[index];
            user.finishedAt = new Date();

            if (users.every(user => user.finishedAt)) {
                io.emit('finished_all', users.sort((a, b) => a.finishedAt - b.finishedAt));
            }
        });

        socket.on("disconnect", () => {
            if (toDelete) {
                users = users.filter(user => user.username !== username);
                socket.broadcast.emit('remove_user', username);
            }
        });
    });
};