export const texts = [
    "Phasellus venenatis vestibulum turpis vel dictum. Donec commodo luctus nunc nec dignissim. Fusce ut ante sed elit tempor auctor non in elit. Donec nec suscipit odio, quis varius mauris. Mauris eget faucibus odio. Donec pretium elementum ullamcorper. Proin enim ligula, sagittis a erat nec, varius semper arcu. Cras vitae quam est. Fusce aliquam tortor eros, id vulputate mauris aliquam sed. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque nulla ligula, rhoncus commodo ornare et, condimentum id arcu.",
    "Nam nec justo euismod purus dapibus cursus. Duis in ipsum ligula. Sed maximus, ex in ultricies molestie, velit massa iaculis diam, a scelerisque magna nisl quis nulla. Maecenas nunc eros.",
    "Nulla facilisi. Integer consequat eros quis nibh tempor, eu lobortis sem aliquet. Nulla lectus turpis, posuere eget sollicitudin et, imperdiet.",
    "Proin quis gravida dui. Vivamus vel venenatis purus, at laoreet massa. Phasellus blandit scelerisque semper. Etiam maximus est non tincidunt placerat. Praesent eget nisl non sapien facilisis blandit sit amet eget nisl. Ut nec semper.",
    "Maecenas eleifend magna sit amet neque luctus efficitur. Donec finibus, ante in porta lobortis, ipsum magna facilisis lorem, eget lobortis erat sem id turpis. Fusce.",
    "Vivamus dignissim tempor consequat. Aliquam nec sapien urna. Suspendisse potenti. Maecenas id vulputate massa. Quisque eu nisi id sem molestie porta. Quisque consequat purus sem, et porta lacus fermentum ac. Cras commodo mauris eget euismod vehicula. Mauris in lectus ante. Phasellus sed mollis purus. Nunc.",
    "Mauris tincidunt mollis neque at suscipit. Duis quis lobortis tellus. Integer non quam quis nunc feugiat tempor nec quis sem. Aliquam erat volutpat. Vivamus ac diam nisi. Vivamus auctor augue metus, et imperdiet.",
];

export default { texts };