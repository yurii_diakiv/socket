import { createElement } from "./helper.mjs";
import { kDownListener } from './game.mjs';

const roomsPage = document.getElementById("rooms-page");

const createUserDiv = (username, myself) => {
    const usersDiv = createElement({
        tagName: "div",
        attributes: { id: username }
    })

    const userDiv = createElement({
        tagName: "div",
        className: "flex user"
    })

    const userP = createElement({
        tagName: "p"
    })

    const readyDiv = createElement({
        tagName: "div",
        className: "is-ready flex-centered ready-div"
    })

    const progressbar = createElement({
        tagName: "progress",
        attributes: {
            value: 0,
            max: 100
        }
    })
    userP.innerText = username;

    if (myself) {
        userP.innerText += ' (you)';
    }

    usersDiv.append(userDiv);
    usersDiv.append(progressbar);
    userDiv.append(readyDiv);
    userDiv.append(userP);


    return usersDiv;
}

export const addUser = username => {
    const newUser = createUserDiv(username);
    roomsPage.append(newUser);
}

export const addUsers = users => {
    users.map(addUser);
}

export const addMyself = username => {
    const newUser = createUserDiv(username, true);
    roomsPage.append(newUser);
}
export const removeUser = username => {
    let userDivToDelete = document.getElementById(username);
    userDivToDelete.remove();
}

export const userError = user => {
    sessionStorage.removeItem('username');
    window.location.replace("/login");
}

export const changeReadyStatus = user => {
    let userDiv = document.getElementById(`${user.username}`);
    let readyDiv = userDiv.childNodes[0].childNodes[0];
    let readyButton = document.getElementById('ready-button');
    let username = sessionStorage.getItem('username');
    if (user.isReady) {
        readyDiv.style.background = '#009933';
        if (user.username === username) {
            readyButton.innerText = 'Not ready';
        }
    } else {
        readyDiv.style.background = '#f50000';
        if (user.username === username) {
            readyButton.innerText = 'Ready';
        }
    }
}

export const updateProgress = (username, textToType, socket) => {
    let text = document.getElementById('text');
    let userDiv = document.getElementById(`${username}`);

    let progressbar = userDiv.childNodes[1];
    let currentValue = progressbar.getAttribute('value');
    let newValue = +currentValue + 1;
    progressbar.setAttribute('value', newValue);

    if (sessionStorage.getItem('username') === username) {
        let firstPart = text.children[0].textContent;
        let secondPart = text.children[1].textContent;
        let thirdPart = text.children[2].textContent;

        text.children[0].textContent = firstPart + secondPart;
        text.children[1].textContent = thirdPart[0];
        text.children[2].textContent = thirdPart.substring(1);

        if (newValue === textToType.length) {
            socket.emit('finished', username);
        }
    }
    if (newValue === textToType.length) {
        progressbar.style.background = 'green';
    }
}

const updateElements = () => {
    let timer2 = document.getElementById('timer2');
    timer2.style.display = 'none';
    document.removeEventListener('keydown', kDownListener);

    let readyDivs = document.getElementsByClassName("ready-div");
    Array.from(readyDivs).map(rd => rd.style.background = '#f50000');

    let readyButton = document.getElementById('ready-button');
    readyButton.style.display = 'flex';
    readyButton.innerText = 'Ready';

    let text = document.getElementById('text');
    text.style.display = 'none';

    let progresses = document.getElementsByTagName("progress");
    Array.from(progresses).map(pr => {
        pr.style.background = '';
        pr.setAttribute('value', 0);
    });

    let disconnectButton = document.getElementById('disconnect-button');
    disconnectButton.style.display = 'block';
}

export const showWinners = users => {
    updateElements();

    console.log(users);
    let message = '';
    for (let i = 0; i < users.length; ++i) {
        message += `${i+1}. ${users[i].username}`;
    }
    alert(message);
}