import {
    addUsers,
    addUser,
    addMyself,
    removeUser,
    userError,
    changeReadyStatus,
    updateProgress,
    showWinners
} from './users.mjs';

const username = sessionStorage.getItem("username");

let textToType = '';

if (!username) {
    window.location.replace("/login");
}

const readybutton = document.getElementById("ready-button");

const onClickReadyButton = () => {
    const username = sessionStorage.getItem('username');
    socket.emit('is_ready', username);
};

readybutton.addEventListener("click", onClickReadyButton);

const atTimerStart = text_id => {
    let readyButton = document.getElementById('ready-button');
    let disconnectButton = document.getElementById('disconnect-button');
    let text = document.getElementById('text');
    let timer = document.getElementById('timer');

    timer.style.display = 'flex';
    readyButton.style.display = 'none';
    disconnectButton.style.display = 'none';

    fetch(`/game/texts/${text_id}`)
        .then(response => response.text())
        .then(data => {
            text.children[1].innerText = data[0];
            text.children[2].innerText = data.substring(1);
            textToType = data;
            let progresses = document.getElementsByTagName("progress");
            Array.from(progresses).map(pr => pr.setAttribute('max', data.length));
        });
}

const updateTimer = counter => {
    let timer = document.getElementById('timer');
    let text = document.getElementById('text');
    let timer2 = document.getElementById('timer2');

    timer.innerText = counter;
    if (counter === 0) {
        timer.style.display = 'none';
        text.style.display = 'block';
        timer2.style.display = 'block';
        addKeyEvent();
    }
}

const updateGameTimer = counter => {
    let timer2 = document.getElementById('timer2');
    timer2.innerText = counter + ' seconds left';
}

const addKeyEvent = () => {
    document.addEventListener('keydown', kDownListener);
}

let index = 0;

export const kDownListener = (event) => {
    if (event.key === textToType[index]) {
        index++;
        socket.emit('press_key', username);
    }
}

const socket = io("", { query: { username } });

socket.on('add_users', addUsers);
socket.on('add_user', addUser);
socket.on('add_myself', addMyself);
socket.on('remove_user', removeUser);
socket.on('user_error', userError);
socket.on('ready', changeReadyStatus);
socket.on('at_timer_start', atTimerStart);
socket.on('timer_before_start', updateTimer);
socket.on('game_timer', updateGameTimer);
socket.on('update_progress', (u) => updateProgress(u, textToType, socket));
socket.on('finished_all', showWinners);