export class User {
    constructor(username, isReady, finishedAt) {
        this.username = username;
        this.isReady = isReady;
        this.finishedAt = finishedAt;
    }
}